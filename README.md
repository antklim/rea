# REA #

This README describes steps are necessary to get `REA` application up and running.

### Set up ###

Clone this repository, go to project folder and install dependencies
```
$ git clone git@bitbucket.org:antklim/rea.git rea
$ cd rea
$ npm i
```

### Start application ###

To start run `npm start` from application folder. Two servers will start (API and front-end).  
API server requires port 3000 to be available and front-end server requires port 3001 to be available. 
Since both servers started navigate to `http://localhost:3001` in your browser.

### API server ###

API server supports the following requests:  

* GET /api/results - returns properties in results  
* GET /api/saved - returns saved properties  
* POST /api/saved - adds property to saved list  
    * id - property's id to add to saved  
* DELETE /api/saved - removes property from saved list  
    * id - property's id to remove from saved list  

### Tests ###

Run `npm t` to start test suite.
`npm run test-backend` to run backend tests only.
`npm run test-frontend` to run frontend tests only.