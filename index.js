'use strict'

const path = require('path');
const db = require('./db');
const reaApp = require('./src/app');

const port = process.env.PORT || 3000;
const payloadPath = process.env.PAYLOAD_PATH || path.join(__dirname, 'db-payload.json');


const startReaApp = () => {
  reaApp.listen(port, err => {
    if (err) {
      return console.error(err);
    }

    console.log(`API server ready on http://localhost:${port}/`);
  });
};


const onDbConnect = err => {
  if (err) {
    return console.error(err);
  }

  startReaApp();
};

// connecting to DB and start application
db.connect(payloadPath, onDbConnect);
