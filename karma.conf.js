const path = require('path')
const webpack = require('webpack')

const webpackCfg = {
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel' ],
        include: [
          path.resolve(__dirname, 'src/actions'),
          path.resolve(__dirname, 'src/components'),
          path.resolve(__dirname, 'src/containers'),
          path.resolve(__dirname, 'src/reducers'),
          path.resolve(__dirname, 'test/frontend')
        ],
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      }
    ]
  },
  externals: {
    'react/addons': true,
    'react/lib/ReactContext': true,
    'react/lib/ExecutionEnvironment': true
  }
}

module.exports = function configureKarma(config) {
  config.set({
    basePath: '',
    browsers: ['PhantomJS'],
    files: [
      'test/frontend/loadtests.js',
    ],
    port: 8080,
    captureTimeout: 60000,
    frameworks: ['phantomjs-shim', 'mocha', 'chai'],
    client: {
      mocha: {},
    },
    singleRun: true,
    reporters: ['mocha'],
    preprocessors: {
      'test/frontend/loadtests.js': ['webpack'],
    },
    webpack: webpackCfg,
    webpackServer: {
      noInfo: true,
    },
  })
}
