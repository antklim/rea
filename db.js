'use strict'

const fs = require('fs');
const assert = require('assert');
const _ = require('lodash');

let tables = {};


const connect = (payloadPath, cb) => {
  if (!payloadPath) {
    return cb(new Error('DB payloadPath required'));
  }

  fs.stat(payloadPath, err => {
    if (err) {
      return cb(err);
    }

    tables = require(payloadPath);
    return cb();
  });
};


const getTables = () => tables;


const create = (options) => {
  options = options || {};
  const table = options.table;
  const payload = options.payload;

  assert(table, 'Table name required');
  assert(payload, 'Payload required');
  assert(tables[ table ], `Table '${table}' does not exist`);

  tables[ table ].push(payload);
  return payload;
};


const read = (options) => {
  options = options || {};
  const table = options.table;
  const query = options.query;

  assert(table, 'Table name required');
  assert(query, 'Query required');
  assert(tables[ table ], `Table '${table}' does not exist`);

  return _.cloneDeep(tables[ table ]);
};


const update = (options) => {
  options = options || {};
  const table = options.table;
  const query = options.query;
  const payload = options.payload;

  throw new Error('Not implemented yet');
};


const _delete = (options) => {
  options = options || {};
  const table = options.table;
  const query = options.query;

  assert(table, 'Table name required');
  assert(query, 'Query required');
  assert(tables[ table ], `Table '${table}' does not exist`);

  const idIndex = _.findIndex(tables[ table ], [ 'id', query ]);

  if (idIndex === -1) {
    return;
  }

  const deleted = tables[ table ].splice(idIndex, 1);
  return deleted[ 0 ];
};


exports.connect = connect;
exports.getTables = getTables;
exports.create = create;
exports.read = read;
exports.update = update;
exports.delete = _delete;
