'use strict'

const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config');

let app = express();
const port = 3001;

const compiler = webpack(config);
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
});

app.listen(port, (err) => {
  if (err) {
    return console.error(error)
  }

  console.log(`Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
});
