'use strict'

const _ = require('lodash');
const db = require('../../db');


const getResults = (query) => {
  const options = {
    table: 'results',
    query: query
  }
  return db.read(options);
}


const getSaved = (query) => {
  const options = {
    table: 'saved',
    query: query
  }
  return db.read(options);
}


const addSaved = (propertyId) => {
  if (_.find(getSaved({}), ['id', propertyId])) {
    return {}
  }

  const options = {
    table: 'saved',
    payload: _.find(getResults({}), ['id', propertyId])
  }
  return db.create(options);
}


const deleteSaved = (propertyId) => {
  const options = {
    table: 'saved',
    query: propertyId
  }
  return db.delete(options);
}


exports.getResults = getResults;
exports.getSaved = getSaved;
exports.addSaved = addSaved;
exports.deleteSaved = deleteSaved;
