import fetch from 'isomorphic-fetch'

export const REQUEST_RESULTS = 'REQUEST_RESULTS'
export const RECEIVE_RESULTS = 'RECEIVE_RESULTS'
export const REQUEST_SAVED = 'REQUEST_SAVED'
export const RECEIVE_SAVED = 'RECEIVE_SAVED'
export const ADD_SAVED = 'ADD_SAVED'
export const DELETE_SAVED = 'DELETE_SAVED'


const API_ROOT = 'http://localhost:3000/api/'


const requestProperties = (category) => {
  let type = null

  switch(category) {
    case 'results':
      type = REQUEST_RESULTS
      break
    case 'saved':
      type = REQUEST_SAVED
      break
    default:
      type = null
      break
  }

  return { type: type }
}


const receiveProperties = (category, json) => {
  let type = null

  switch(category) {
    case 'results':
      type = RECEIVE_RESULTS
      break
    case 'saved':
      type = RECEIVE_SAVED
      break
    default:
      type = null
      break
  }

  return {
    type: type,
    items: json,
    receivedAt: Date.now()
  }
}


const fetchProperties = (category) => {
  return dispatch => {
    dispatch(requestProperties(category))
    return fetch(`${API_ROOT}${category}`)
      .then(response => response.json())
      .then(json => dispatch(receiveProperties(category, json)))
  }
}


const shouldFetchProperties = (state, category) => {
  const properties = state[category]

  if (!properties.items.length) {
    return true
  }

  if (properties.isFetching) {
    return false
  }

  return properties.didInvalidate
}


// category: [results, saved]
export const fetchPropertiesIfNeeded = (category) => {
  return (dispatch, getState) => {
    if (shouldFetchProperties(getState(), category)) {
      return dispatch(fetchProperties(category))
    }
  }
}


const receiveAddToSavedResponse = payload => {
  return { type: ADD_SAVED, payload }
}


export const addToSaved = (id) => {
  const category = 'saved'
  return dispatch => {
    return fetch(`${API_ROOT}${category}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({propertyId: id})
    })
    .then(response => response.json())
    .then(json => dispatch(receiveAddToSavedResponse(json)))
  }
}


const receiveDeleteFromSavedResponse = deletedSaved => {
  return { type: DELETE_SAVED, id: deletedSaved.id }
}


export const deleteFromSaved = (id) => {
  const category = 'saved'
  return dispatch => {
    return fetch(`${API_ROOT}${category}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({propertyId: id})
    })
    .then(response => response.json())
    .then(json => dispatch(receiveDeleteFromSavedResponse(json)))
  }
}
