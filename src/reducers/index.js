import { combineReducers } from 'redux'
import results from './results'
import saved from './saved'


export default combineReducers({
  results,
  saved
})
