import { isEmpty } from 'lodash';
import { REQUEST_SAVED, RECEIVE_SAVED, ADD_SAVED, DELETE_SAVED } from '../actions'


const saved = (state = {
  isFetching: false,
  didInvalidate: false,
  items: []
}, action) => {
  switch (action.type) {
    case REQUEST_SAVED:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_SAVED:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.items,
        lastUpdated: action.receivedAt
      })
    case ADD_SAVED:
      if (isEmpty(action.payload)) {
        return state
      }

      return Object.assign({}, state, {
        items: state.items.concat(action.payload)
      })
    case DELETE_SAVED:
      return Object.assign({}, state, {
        items: state.items.filter(s => s.id !== action.id)
      })
    default:
      return state
  }
}


export default saved
