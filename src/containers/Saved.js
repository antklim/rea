import { connect } from 'react-redux'
import { deleteFromSaved, fetchPropertiesIfNeeded } from '../actions'
import PropertyColumn from '../components/PropertyColumn'


const mapStateToProps = (state) => {
  return {
    properties: state.saved.items
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onPropertyClick: id => {
      dispatch(deleteFromSaved(id))
    },
    loadData: () => {
      dispatch(fetchPropertiesIfNeeded('saved'))
    }
  }
}


const Saved = connect(
  mapStateToProps,
  mapDispatchToProps
)(PropertyColumn)


export default Saved
