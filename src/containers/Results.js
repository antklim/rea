import { connect } from 'react-redux'
import { addToSaved, fetchPropertiesIfNeeded } from '../actions'
import PropertyColumn from '../components/PropertyColumn'


const mapStateToProps = (state) => {
  return {
    properties: state.results.items
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onPropertyClick: id => {
      dispatch(addToSaved(id))
    },
    loadData: () => {
      dispatch(fetchPropertiesIfNeeded('results'))
    }
  }
}


const Results = connect(
  mapStateToProps,
  mapDispatchToProps
)(PropertyColumn)


export default Results
