'use strict'

const express = require('express');
const lib = require('../lib');

const router = express.Router();


/* results routes */
router.get('/results', (req, res, next) => {
  res.send(lib.getResults({}));
});


/* saved routes */
router.get('/saved', (req, res, next) => {
  res.send(lib.getSaved({}));
});

router.post('/saved', (req, res, next) => {
  res.send(lib.addSaved(req.body.propertyId));
});

router.delete('/saved', (req, res, next) => {
  res.send(lib.deleteSaved(req.body.propertyId));
});

module.exports = router;
