import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './components/App'
import configureStore from './store/configureStore'


const initialState = {
  results: {
    isFetching: false,
    didInvalidate: false,
    items: []
  },
  saved: {
    isFetching: false,
    didInvalidate: false,
    items: []
  }
}

const store = configureStore(initialState)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
