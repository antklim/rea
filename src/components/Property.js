import React, { Component, PropTypes } from 'react'

require('../styles/Property.css')

class Property extends Component {
  constructor(props) {
    super(props)

    this.state = { showHoverButton: false }

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter(e) {
    this.setState({ showHoverButton: true })
  }

  onMouseLeave(e) {
    this.setState({ showHoverButton: false })
  }

  render() {
    const { mainImage, agency, price, hoverButtonText, onClick } = this.props

    return (
      <li className="propertyContainer">
        <div className="property">
          <div className="header" style={{backgroundColor: agency.brandingColors.primary}}>
            <img src={agency.logo} />
          </div>
          <div className="body" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
            <img src={mainImage} />
            <div className="hoverButton" style={{display: this.state.showHoverButton ? "" : "none"}}></div>
            <div
              className="hoverButtonText"
              style={{display: this.state.showHoverButton ? "" : "none"}}
              onClick={onClick}
            >{hoverButtonText}</div>
          </div>
          <div className="footer">
            {price}
          </div>
        </div>
      </li>
    )
  }
}

Property.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  mainImage: PropTypes.string.isRequired,
  agency: PropTypes.shape({
    brandingColors: PropTypes.shape({
      primary: PropTypes.string.isRequired
    }).isRequired,
    logo: PropTypes.string.isRequired
  }).isRequired,
  hoverButtonText: PropTypes.string.isRequired
}


export default Property
