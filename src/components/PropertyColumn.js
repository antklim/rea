import React, { Component, PropTypes } from 'react'
import PropertyList from './PropertyList'

require('../styles/PropertyColumn.css')

class PropertyColumn extends Component {
  componentDidMount() {
    this.props.loadData()
  }

  render() {
    const { title, properties, hoverButtonText, onPropertyClick } = this.props
    return (
      <div className="propertyColumn">
        <div className="title">{title}</div>
        <PropertyList
          properties={properties}
          hoverButtonText={hoverButtonText}
          onPropertyClick={onPropertyClick}
        />
      </div>
    )
  }
}


PropertyColumn.propTypes = {
  properties: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    mainImage: PropTypes.string.isRequired,
    agency: PropTypes.shape({
      brandingColors: PropTypes.shape({
        primary: PropTypes.string.isRequired
      }).isRequired,
      logo: PropTypes.string.isRequired
    }).isRequired
  }).isRequired).isRequired,
  title: PropTypes.string.isRequired,
  hoverButtonText: PropTypes.string.isRequired,
  onPropertyClick: PropTypes.func.isRequired,
  loadData: PropTypes.func.isRequired
}


export default PropertyColumn
