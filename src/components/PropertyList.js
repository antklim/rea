import React, { PropTypes } from 'react'
import Property from './Property'

require('../styles/PropertyList.css')

const PropertyList = ({ properties, hoverButtonText, onPropertyClick }) => (
  <div className="propertyList">
    <ul className="list">
      {properties.map(property =>
        <Property
          key={property.id}
          {...property}
          hoverButtonText={hoverButtonText}
          onClick={() => onPropertyClick(property.id)}
        />
      )}
    </ul>
  </div>
)


PropertyList.propTypes = {
  properties: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    mainImage: PropTypes.string.isRequired,
    agency: PropTypes.shape({
      brandingColors: PropTypes.shape({
        primary: PropTypes.string.isRequired
      }).isRequired,
      logo: PropTypes.string.isRequired
    }).isRequired,
  }).isRequired).isRequired,
  hoverButtonText: PropTypes.string.isRequired,
  onPropertyClick: PropTypes.func.isRequired
}


export default PropertyList
