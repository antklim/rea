require('normalize.css')
require('../styles/App.css')

import React from 'react'
import Results from '../containers/Results'
import Saved from '../containers/Saved'


const App = () => (
  <div className="layout container">
    <Results title="Results" hoverButtonText="Add To Saved" />
    <Saved title="Saved Properties" hoverButtonText="Remove From Saved" />
  </div>
)


export default App
