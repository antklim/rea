'use strict'

const path = require('path');
const assert = require('assert');
const request = require('supertest');
const db = require('../../db');
const api = require('../../src/app');

const payloadPath = path.join(__dirname, '../../db-payload.json');


describe('api', () => {

  before(done => {
    db.connect(payloadPath, done);
  });

  it('GET /api/results', done => {
    request(api)
      .get('/api/results')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, (err, res) => {
        assert.ifError(err);
        assert(res.body)

        const results = res.body;
        assert(results.length > 0);

        const result = results[ 0 ];
        assert(result.id);
        assert(result.price);
        assert(result.agency);
        assert(result.mainImage);

        done();
      });
  });

  it('GET /api/saved', done => {
    request(api)
      .get('/api/saved')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, (err, res) => {
        assert.ifError(err);
        assert(res.body)

        const saved = res.body;
        assert(saved.length > 0);

        const savedItem = saved[ 0 ];
        assert(savedItem.id);
        assert(savedItem.price);
        assert(savedItem.agency);
        assert(savedItem.mainImage);

        done();
      });
  });

  it('POST /api/saved', done => {
    const payload = {
      propertyId: '3'
    }
    request(api)
      .post('/api/saved')
      .set('Accept', 'application/json')
      .send(payload)
      .expect('Content-Type', /json/)
      .expect(200, (err, res) => {
        assert.ifError(err);
        assert(res.body)
        done();
      });
  });

  it('DELETE /api/saved', done => {
    const payload = {
      propertyId: '4'
    }
    request(api)
      .delete('/api/saved')
      .set('Accept', 'application/json')
      .send(payload)
      .expect('Content-Type', /json/)
      .expect(200, (err, res) => {
        assert.ifError(err);
        assert(res.body)
        done();
      });
  });
});
