'use strict'

const path = require('path');
const assert = require('assert');
const _ = require('lodash');
const db = require('../../db');
const lib = require('../../src/lib');

const payloadPath = path.join(__dirname, '../../db-payload.json');


describe('lib', () => {

  before(done => {
    db.connect(payloadPath, done);
  });

  it('should return `results`', () => {
    const results = lib.getResults({});
    assert(results);
    assert(results.length > 0);

    const result = results[ 0 ];
    assert(result.id);
    assert(result.price);
    assert(result.agency);
    assert(result.mainImage);
  });

  it('should return `saved`', () => {
    const saved = lib.getSaved({});
    assert(saved);
    assert(saved.length > 0);

    const savedItem = saved[ 0 ];
    assert(savedItem.id);
    assert(savedItem.price);
    assert(savedItem.agency);
    assert(savedItem.mainImage);
  });

  it('should add to saved', () => {
    const propertyId = '1'
    const savedBefore = lib.getSaved({});
    assert.equal(_.findIndex(savedBefore, [ 'id', propertyId ]), -1);

    let added = lib.addSaved(propertyId);

    const savedAfter = lib.getSaved({});
    const savedIndex = _.findIndex(savedAfter, [ 'id', propertyId ]);

    assert(savedIndex > -1);

    added = lib.addSaved(propertyId);
    assert.deepEqual(added, {});
  });

  it('should delete from saved', () => {
    const propertyId = '2'
    lib.addSaved(propertyId);

    const savedBefore = lib.getSaved({});
    let savedIndex = _.findIndex(savedBefore, [ 'id', propertyId ]);
    assert(savedIndex > -1);

    const deleted = lib.deleteSaved(propertyId);

    const savedAfter = lib.getSaved({});
    savedIndex = _.findIndex(savedAfter, [ 'id', propertyId ]);
    assert.equal(savedIndex, -1);
  });
});
