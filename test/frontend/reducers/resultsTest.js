import reducer from '../../../src/reducers/results'
import { REQUEST_RESULTS, RECEIVE_RESULTS } from '../../../src/actions'

describe('Reducers - results', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: []
    })
  })

  it('should handle REQUEST_RESULTS', () => {
    expect(reducer(undefined, { type: REQUEST_RESULTS })).to.deep.equal({
      isFetching: true,
      didInvalidate: false,
      items: []
    })
  })

  it('should handle RECEIVE_RESULTS', () => {
    expect(reducer(undefined, {
      type: RECEIVE_RESULTS,
      items: [ { id: 1 } ],
      receivedAt: '3000-01-01'
    })).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 1 } ],
      lastUpdated: '3000-01-01'
    })
  })
})
