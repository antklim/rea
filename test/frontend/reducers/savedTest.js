import reducer from '../../../src/reducers/saved'
import { REQUEST_SAVED, RECEIVE_SAVED, ADD_SAVED, DELETE_SAVED } from '../../../src/actions'

describe('Reducers - saved', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: []
    })
  })

  it('should handle REQUEST_SAVED', () => {
    expect(reducer(undefined, { type: REQUEST_SAVED })).to.deep.equal({
      isFetching: true,
      didInvalidate: false,
      items: []
    })
  })

  it('should handle RECEIVE_SAVED', () => {
    expect(reducer(undefined, {
      type: RECEIVE_SAVED,
      items: [ { id: 1 } ],
      receivedAt: '3000-01-01'
    })).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 1 } ],
      lastUpdated: '3000-01-01'
    })
  })

  it('should handle ADD_SAVED with empty payload', () => {
    expect(reducer(undefined, {
      type: ADD_SAVED,
      payload: []
    })).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: []
    })
  })

  it('should handle ADD_SAVED with payload', () => {
    expect(reducer({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 1 } ]
    }, {
      type: ADD_SAVED,
      payload: [ { id: 2 } ]
    })).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 1 }, { id: 2 } ]
    })
  })


  it('should handle DELETE_SAVED', () => {
    expect(reducer({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 1 }, { id: 2 } ]
    }, {
      type: DELETE_SAVED,
      id: 1
    })).to.deep.equal({
      isFetching: false,
      didInvalidate: false,
      items: [ { id: 2 } ]
    })
  })
})
