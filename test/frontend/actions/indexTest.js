import configureMockStore from 'redux-mock-store'
import fetchMock from 'fetch-mock'
import * as actions from '../../../src/actions'

const mockStore = configureMockStore()

describe('Actions', () => {
  let store = null

  beforeEach(() => {
    store = mockStore({
      results: {
        isFetching: false,
        didInvalidate: false,
        items: []
      },
      saved: {
        isFetching: false,
        didInvalidate: false,
        items: []
      }
    })
  })

  it('dispatches RECEIVE_RESULTS')
  it('dispatches RECEIVE_SAVED')
  it('dispatches ADD_SAVED')
  it('dispatches DELETE_SAVED')
})
