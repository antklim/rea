import React from 'react'
import { shallow, mount } from 'enzyme'
import PropertyList from '../../../src/components/PropertyList'
import PropertyColumn from '../../../src/components/PropertyColumn'

describe('<PropertyColumn />', () => {
  const hoverButtonText = 'Click me'
  const payload = [{
    id: '1',
    price: '$726,500',
    mainImage: '',
    agency: {
      brandingColors: {
        primary: '#ffe512'
      },
      logo: ''
    }
  }]
  const title = 'Column A'


  it('should render a column with list of properties', () => {
    const component = shallow(<PropertyColumn
      properties={payload}
      title={title}
      hoverButtonText={hoverButtonText}
      onPropertyClick={() => {}}
      loadData={() => {}}
    />)

    expect(component.props().className).to.equal('propertyColumn')
    expect(component.contains(<div className='title'>Column A</div>)).to.be.true
    expect(component.find(PropertyList)).to.have.length(1)
  })

  it('should load data when mounted', () => {
    let dataLoaded = false
    const component = mount(<PropertyColumn
      properties={payload}
      title={title}
      hoverButtonText={hoverButtonText}
      onPropertyClick={() => {}}
      loadData={() => { dataLoaded = true }}
    />)

    expect(dataLoaded).to.be.true
  })
})
