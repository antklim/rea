import React from 'react'
import { shallow } from 'enzyme'
import Property from '../../../src/components/Property'

describe('<Property />', () => {
  const hoverButtonText = 'Click me'
  const payload = {
    id: '1',
    price: '$726,500',
    mainImage: 'mainImagePath',
    agency: {
      brandingColors: {
        primary: '#ffe512'
      },
      logo: 'logoPath'
    }
  }

  const indexes = {
    header: 0,
    body: 1,
    footer: 2,
  }


  it('should render property', () => {
    const component = shallow(<Property
      onClick={() => {}}
      id={payload.id}
      price={payload.price}
      mainImage={payload.mainImage}
      agency={payload.agency}
      hoverButtonText={hoverButtonText}
    />)

    expect(component.state('showHoverButton')).to.be.false
    expect(component.instance().onMouseEnter).to.be.a('function')
    expect(component.instance().onMouseLeave).to.be.a('function')

    const root = component.find('li.propertyContainer')
    expect(root).to.have.length(1)
    expect(root.find('div.property')).to.have.length(1)

    const rootChildren = root.find('div.property').children()
    expect(rootChildren).to.have.length(3)

    // header
    const header = rootChildren.at(indexes.header)
    expect(header.prop('className')).to.equal('header')
    expect(header.prop('style')).to.deep.equal({backgroundColor: '#ffe512'})

    const headerImages = header.children()
    expect(headerImages).to.have.length(1)
    expect(headerImages.at(0).props()).to.deep.equal({src: 'logoPath'})

    // body
    const body = rootChildren.at(indexes.body)
    expect(body.prop('className')).to.equal('body')
    expect(body.children()).to.have.length(3)

    const bodyImage = body.children().at(0)
    expect(bodyImage.props()).to.deep.equal({src: 'mainImagePath'})

    const hoverButton = body.children().at(1)
    expect(hoverButton.props()).to.deep.equal({className: 'hoverButton', style: {display: 'none'}})

    const hoverButtonTxt = body.children().at(2)
    expect(hoverButtonTxt.prop('className')).to.equal('hoverButtonText')
    expect(hoverButtonTxt.prop('style')).to.deep.equal({display: 'none'})
    expect(hoverButtonTxt.prop('onClick')).to.be.a('function')
    expect(hoverButtonTxt.contains('Click me')).to.be.true

    // footer
    const footer = rootChildren.at(indexes.footer)
    expect(footer.props()).to.deep.equal({className: 'footer', children: '$726,500'})
  })

  it('should render hover button when mouse is over property', () => {
    let clickCounter = 0
    const component = shallow(<Property
      onClick={() => { clickCounter++ }}
      id={payload.id}
      price={payload.price}
      mainImage={payload.mainImage}
      agency={payload.agency}
      hoverButtonText={hoverButtonText}
    />)
    expect(component.state('showHoverButton')).to.be.false

    let body = component.find('li.propertyContainer').find('div.property').children().at(indexes.body)
    let hoverButton = body.children().at(1)
    let hoverButtonTxt = body.children().at(2)
    expect(hoverButton.prop('style')).to.deep.equal({display: 'none'})
    expect(hoverButtonTxt.prop('style')).to.deep.equal({display: 'none'})

    body.simulate('mouseEnter')
    expect(component.state('showHoverButton')).to.be.true

    body = component.find('li.propertyContainer').find('div.property').children().at(indexes.body)
    hoverButton = body.children().at(1)
    hoverButtonTxt = body.children().at(2)
    expect(hoverButton.prop('style')).to.deep.equal({display: ''})
    expect(hoverButtonTxt.prop('style')).to.deep.equal({display: ''})

    hoverButtonTxt.simulate('click')
    expect(clickCounter).to.equal(1)
  })

  it('should hide hover button when mouse leaves property', () => {
    const component = shallow(<Property
      onClick={() => {}}
      id={payload.id}
      price={payload.price}
      mainImage={payload.mainImage}
      agency={payload.agency}
      hoverButtonText={hoverButtonText}
    />)

    component.setState({ showHoverButton: true })

    let body = component.find('li.propertyContainer').find('div.property').children().at(indexes.body)
    let hoverButton = body.children().at(1)
    let hoverButtonTxt = body.children().at(2)
    expect(hoverButton.prop('style')).to.deep.equal({display: ''})
    expect(hoverButtonTxt.prop('style')).to.deep.equal({display: ''})

    body.simulate('mouseLeave')
    expect(component.state('showHoverButton')).to.be.false

    body = component.find('li.propertyContainer').find('div.property').children().at(indexes.body)
    hoverButton = body.children().at(1)
    hoverButtonTxt = body.children().at(2)
    expect(hoverButton.prop('style')).to.deep.equal({display: 'none'})
    expect(hoverButtonTxt.prop('style')).to.deep.equal({display: 'none'})
  })
})
