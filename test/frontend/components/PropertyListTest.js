import React from 'react'
import { shallow } from 'enzyme'
import Property from '../../../src/components/Property'
import PropertyList from '../../../src/components/PropertyList'

describe('<PropertyList />', () => {
  const hoverButtonText = 'Click me'
  const payload = [
    {
      id: '1',
      price: '$726,500',
      mainImage: '',
      agency: {
        brandingColors: {
          primary: '#ffe512'
        },
        logo: ''
      }
    },
    {
      id: '2',
      price: '$726,500',
      mainImage: '',
      agency: {
        brandingColors: {
          primary: '#ffe512'
        },
        logo: ''
      }
    }
  ]


  it('should render a list of properties', () => {
    const component = shallow(<PropertyList
      properties={payload}
      hoverButtonText={hoverButtonText}
      onPropertyClick={() => {}}
    />)

    expect(component.prop('className')).to.equal('propertyList')
    expect(component.children()).to.have.length(1)

    const list = component.children().at(0)
    expect(list.prop('className')).to.equal('list')
    expect(list.find(Property)).to.have.length(2)
  })
})
